Realync.Models.LiveTourChat = Backbone.Model.extend({
    initialize: function() {
        this.set("time", this.setTimeForChat())
    },

    setTimeForChat: function(){
        var time = this.get("timestamp").split("T")[1].split(":")[0] + ":" + this.get("timestamp").split("T")[1].split(":")[1]
        return time;
    },

    url: 'http://localhost:3001/dashboard'
});
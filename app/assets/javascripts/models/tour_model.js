Realync.Models.Tour = Backbone.Model.extend({
    initialize: function() {
        _.bindAll(this, 'getHours');
        this.setPropertyParams(this);
        tour_model = this;
        this.set('word_data', this.setWordData())
        this.set('fullAdress', this.setPropertyParams());
        this.set('fullRating', this.setFullRating());
        this.set('hours', this.get_time(this.get("startDate"), "global"));
    },

    schema: {
        propertyID: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Add property'
            },
            title: null
        },
        attendees: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Invite attendees'

            },
            title: null
        },
        dateTime: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Date                                                          Time',
                'disabled' : true
            },
            title: null
        }
    },

    setWordData: function () {
        var date = new Date(this.get('startDate'));
        var month = '';
        var day = '';
        if(date){
           month = this.get_month(date.getMonth());
           day = this.get_day(date.getDay());
        }


        return day + ', ' + month + ' ' + date.getDate()
    },
    get_month: function(number){
        var month = ['', "January", "February", "March", "April", "May", "June", "July", "August", "September",
                     "October", "November", "December"  ];
        var n = month[number];
        return n
    },

    get_day: function(number){
        var week = [ "Sunday", "Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        var n = week[number];
        return n
    },

    upcomingTourAgent: function (type) {
        $.ajax({
//            url: localStorage.url + '/tour/upcomingAgentTours',
            url: localStorage.url + '/tour/index',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
                var upcoming_tour = output.tours;
                for(idx in upcoming_tour) {
                    var upcoming = new Realync.Models.Tour(upcoming_tour[idx]);
                    Realync.upcoming_collection.add(upcoming);
                    var view = new Realync.Views.agentDashboard;
                    view.addAllUpcomingTours();
                };
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    rateTour: function (videoID, rating) {
        console.log("==============ADD Rating===================")

        $.ajax({
            url: localStorage.url + '/tour/rateTour',

            type: 'POST',

            data: {videoID: videoID,
                   rating: rating
                  },

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log(output);
            },
            error: function(data){
                console.log('Error update property');
            }
        })
    },

    setFullRating: function() {
        var thisTotal = 0;
        var rating = 0;
        var arr_rating = this.get('ratings');

        if(arr_rating) {
            for(var i=0;i<arr_rating.length;i++) {
                thisTotal+=arr_rating[i].rating;
            }
            
            rating=Math.round(thisTotal/arr_rating.length);
        }
        return rating;
    },

    startArchiving: function (sessionID, name) {
        console.log("-==================START ARCHIVING====================")

        $.ajax({
            url: localStorage.url + '/tour/startArchive',

            type: 'GET',

            data: {sessionId: sessionID,
                   name: name
            },

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log(output);
                console.log(output.id);
                localStorage.arhivingID = output.id;
            },
            error: function(data){
                console.log('Error update property');
            }
        })
    },


    stopArchiving: function (tour, session, finished_data, save_data) {
        console.log("==================STOP ARCHIVING====================")


        $.ajax({
            url: localStorage.url + '/tour/stopArchive/',

            type: 'GET',

            data: {archiveId: localStorage.arhivingID},

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log(output);
                tour_model.tourFinished(finished_data, save_data);

                session.disconnect();

            },
            error: function(data){
                session.disconnect();
            }
        })
    },


    showVideo: function (id) {

        $.ajax({
            url: localStorage.url + '/video/show/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                var tour = new Realync.Models.Tour(output.video);
                var view = new Realync.Views.ShowTour({model: tour});
                view.render();
                console.log('111111111111111111111111');
                console.log(output.video);
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

   deleteVideo: function (id) {
       console.log('----------------------delete---video----------------');
        $.ajax({
            url: localStorage.url + '/video/delete',

            type: 'POST',
            data: { videoID: id },
            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log('111111111111111111111111');
                console.log(output);
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

   tourFinished: function (finished_data, save_data) {
        console.log("==================TOUR IS FINISHED====================")

        $.ajax({
            url: localStorage.url + '/tour/tourFinished',
            type: 'POST',

            data: finished_data,

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                tour_model.tourSave(save_data);
                window.location = "dashboard#!/"
            },
            error: function(data){
                console.log('Error update property');
            }
        })
    },

    tourSave: function (save_data, agent) {
        console.log("==================SAVE TOUR====================")


        $.ajax({
            url: localStorage.url + '/user/saveTour',
            type: 'POST',

            data: save_data,

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log(output);
                window.location = "dashboard#!/"
                if(agent){
                    tour_model.deleteTour(save_data.tourID);
                }

            },
            error: function(data){
                console.log('Error update property');
            }
        })
    },



    clientTours: function (type) {
        $.ajax({
            url: localStorage.url + '/tour/index',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log('111111111111111111111111');
                var client_tours = Realync.client_tours_collection;
                client_tours.reset();

                for (i in output.tours){
                    var client_tour_model = new Realync.Models.Tour(output.tours[i]);
                    client_tours.add(client_tour_model);
                }

                console.log(output);
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    inviteUserForTour: function(email, full_name){
        $.ajax({
            url: localStorage.url + '/tour/invite',

            type: 'POST',
            data: {email: email},

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                var li = '<div>';
                li += "<div class='movie-result'>";
                li += "<div class='attendee-wrap'>";
                li += "<img class='round' src='" + "/images/noPhoto.jpg" + "'/>";
                li += "<div class='user-name' >" + full_name + "</div>";
                li += "<div class='user-id'><div style='visibility:hidden' id='" + output.createdUser + "'></div></div>";
                li += "</div>";
                li += "<div class='remove-attendee' id='" + output.createdUser + "'><img class='remove-icon' src='" + "assets/remove-attendee.png" + "'/></div>";
                li += '</div>';

                $('div.attendees-container').append(li);

                $('.remove-attendee').click(function(){
                    var attendees = $('input[name="attendees"]').val();
                    var spl = attendees.split(',');
                    var user_id = $(this).attr('id');
                    spl.forEach(function(val) {
                        if(val == user_id) {
                            spl.splice(spl.indexOf(val), 1);
                        }
                    })
                    $('input[name="attendees"]').val(spl.join(','));
                    $(this).parent('.movie-result').remove();
                });

                console.log(output);
            },
            error: function(data){
                console.log(data);
            }

        })
    },

    getClientTours: function(id) {
        $.ajax({
            url: localStorage.url + '/tour/clientTours/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data){
                console.log(data);
                var tours = data.tours;
                Realync.upcoming_collection.reset();
                for (idx in tours) {
                    var tour = new Realync.Models.Tour(tours[idx]);
                    Realync.upcoming_collection.add(tour);
                }
                if (data.status == "failed") {
                    $('.without_tours').css({"display":"block"})
                } else {
                    var view = new Realync.Views.upcoming_tours_list;
                    view.render();
                }


            },
            error: function(data){
                console.log('Error');
            }
        })
    },

    createTour: function(data_create){
        $.ajax({
           url: localStorage.url + '/tour/create',
           type: 'POST',
           data: data_create,
           xhrFields: {
             withCredentials: true
           },

           success: function(data){
               if(data_create.isOpenHouse){
                   var openHouseURL = data.openHouseURL;
                   tour_model.share_open_house_by_facebook(openHouseURL, data_create.propertyID);
                   tour_model.share_open_house_by_twitter(openHouseURL, data_create.propertyID);
               }

               window.location = '#!/upcoming'
           },

           error: function(data){
             console.log("error");
             console.log(data);
           }
        });
    },

    editTour: function(data, id){

        $.ajax({
            url: localStorage.url + '/tour/edit/' + id,
            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },

            success: function(data){
                window.location = "#!/upcoming"
            },

            error: function(data){
                console.log(data);
            }
        });
    },

    share_open_house_by_facebook: function(url_facebook, id){
        if($("#facebook").prop('checked') == true){
            var property = _.find(Realync.properties_collection.models, function(property){return property.get("id") == id})


            FB.ui({
                method: 'feed',
                name: property.get("searchString"),
                link: url_facebook,
                picture: property.get('picURL'),
                caption: 'BED: ' + property.get("bedroomCount") + ', ' + 'BATH: ' + property.get("bathCount") + ', ' +
                    'SQ FEET: ' +  property.get("sqft") ,
                description: property.get('description'),
                message: ''
            });

        }
    },


    share_open_house_by_twitter: function(url_twitter, id){
        if($("#twitter").prop('checked') == true){
            var property = _.find(Realync.properties_collection.models, function(property){return property.get("id") == id})
            var url = 'https://twitter.com/intent/tweet?';
            url +=  'text=' + encodeURIComponent(property.get("searchString") + "\n ")

            url +=  '&url='+encodeURIComponent(url_twitter);
            var left  = ($(window).width()/2)-(600/2), top   = ($(window).height()/2)-(300/2)
            popup = window.open (url, "popup", "width=600, height=300, top="+top+", left="+left);
        }
    },

    deleteTour: function (id) {
        console.log('Ok');
        $.ajax({
            url: localStorage.url + '/tour/delete',
            type: 'POST',
            data: {tourID: id},
            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
            },
            error: function(data){
                console.log('Error update property');
            }

        })

    },

    setPropertyParams: function(){
        var addr_string = this.get("address");

        if(addr_string){
            return (addr_string.street1 + " " + addr_string.city + " " + addr_string.state + " " + addr_string.zip)
        }else{
            return "No property"
        }
    },

    get_time: function(data, type){
        var d = new Date(data);
        var hour = '';
        var am = '';
        if(d.getHours() - 12 > 0){
            hour = d.getHours() - 12;
            if(type == "calendar") am = 'p'
            else am = 'PM';
        }else{
            hour = d.getHours();
            if(type == "calendar") am = 'a'
            else am = 'AM';
        }
        if(type == "calendar") {
            return  hour + ':' + (d.getMinutes() < 10 ? ('0' + d.getMinutes()) : d.getMinutes()) +am;
        } else {      
            return  hour + ':' + (d.getMinutes() < 10 ? ('0' + d.getMinutes()) : d.getMinutes()) + ' ' +am;
        } 
    },

    setCalendar: function(agent_tours, type){
        agent_tours.each(function(model, index){
            var photo = "";

            if(model.attributes.property){
                photo = model.attributes.property.mobilePicURL;
            }
            var start_time = tour_model.get_time(model.get('startDate'), "calendar");
            $("#calendar").fullCalendar('renderEvent', { 
                title: model.get("fullAdress"),
                start: model.get('startDate'),
                start_time: start_time,
                model: model,
                property_url: photo,
                allDay: true 
            }, true );
        })
    },

    agentTour: function(){
        console.log("agent Tour");

        $.ajax({
            url: localStorage.url + '/tour/agentTours',
            url: localStorage.url + '/tour/index',
            type: 'GET',
            xhrFields: {
                withCredentials: true
            },
            success: function(data){
                console.log("Ok");
                console.log(data);
                var agent_tours = Realync.agent_tour_collection;
                agent_tours.reset();

                for(index in data.tours){
                    if(data.tours[index].property){
                        var tour_i = new Realync.Models.Tour(data.tours[index]);
                        agent_tours.add(tour_i);
                    }else if(data.tours[index].address && Object.keys(data.tours[index].address).length  == 1){
                        data.tours[index].without_property = true;
                        var tour_i = new Realync.Models.Tour(data.tours[index]);
                        agent_tours.add(tour_i);
                    }
                }
                tour_model.setCalendar(agent_tours, 'upcoming');
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    },

    agentPastTours: function () {
        $.ajax({
            url: localStorage.url + '/video/index',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(data){
                console.log("Ok");
                console.log(data);
                var agent_tours = Realync.past_tours_collection;
                agent_tours.reset();

                for(index in data.videos){
                    if(data.videos[index].property){
                        data.videos[index].past = true
                        var tour_i = new Realync.Models.Tour(data.videos[index]);
                        agent_tours.add(tour_i);
                    }
                }
                tour_model.setCalendar(agent_tours, 'past');
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }

        })

    },

    getHours: function(m) {
        console.log(m);
        var h = Math.floor(m/60);
        m -= h*60;
        return ":00 "+(h > 0 ? '-' : '+')+(h < 10 ? '0' + Math.abs(h) : Math.abs(h))+(m < 10 ? '0'+m : m);
    },

    url: 'http://localhost:3001/dashboard'
});
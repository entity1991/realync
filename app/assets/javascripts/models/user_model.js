Realync.Models.User = Backbone.Model.extend({
    initialize: function() {
        user_model = this;

        _.bindAll(this, 'sign_up');
        this.set("fullAddress", this.setPropertyAddress());
    },
  defaults: {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    location: '',
    password: '',
    role: '',
    avatar: ''
  },

    schema: {
        isRealtor: {
            type: 'Radio', options: [{val: 'false', label: 'new_client'},{val: true, label: 'new_agent'}],
            title: '',
            editorAttrs: {
                'id': 'form_name'
            }
//            label:'roles_123'
        },

        firstName: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'First name'
            },
            title: null

        },
        lastName: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Last name'
            },
            title: null

        },
        searchRadius: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': '#',
                'class': 'searching'
            },
            title: null


        },
        zipcode: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Zip code'
            },
            title: null
        },

        bedCount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        bathcount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        sqft: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        priceMax: {
            editorAttrs: {
                'placeholder': 'Max'
            },
            title: null
        },

        priceMin: {
            editorAttrs: {
                'placeholder': 'Min'
            },
            title: null
        },

        description: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': "Describe What You're Looking For"
            },
            title: null
        },
        phoneNumber: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Zip code'
            },
            title: null
        },

        email: {
            validators: ['required', 'email'],
            editorAttrs: {
                'placeholder': 'Email'
            },
            title: null
        },
        mobile: {
            type: 'Hidden',

            editorAttrs: {
                'placeholder': 'Password'
            },
            title: null,
            value: false
        },

        confirmation: {
            type: 'Password',
            validators: ['required', { type: 'match', field: 'password', message: 'Passwords must match!' }],

            editorAttrs: {
                'placeholder': 'Confirmation new password'
            },
            title: null
        },

        password: {
            type: 'Password',
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Password'
            },
            title: null
        },

        current_password: {
            type: 'Password',
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Old Password'
            },
            title: null
        }
    },
  url: 'http://localhost:3001/dashboard',

    sign_up: function(data, render){

        console.log( 'sign_up--------------------' );
        console.log( data);
        data.accessCode = 'realyncBeta'

        $.ajax({
            url: localStorage.url + '/user/create',

            type: 'POST',
            xhrFields: {
                withCredentials: true
            },
            data: data,
            success: function(data_r){
                console.log(data );
                console.log(data_r );
                if(data_r.status == 'SUCCESS'){
                    var user =  data_r.session.User;
                    localStorage.user = JSON.stringify(user);
                    if(render){
                        this.sign_up = true;
                        user_model.get_params_by_zip( data,'client')
                    }else{
                        this.sign_up = "none";

                        user_model.get_params_by_zip( data,'agent');
                       // window.location = "/dashboard#!/";
                    }

//
                }else{
                    console.log('sign up bad' );
                    if(data_r.status == 'failed'){
                       $('.user-api-errors').html(data_r.message)
                    }
                };
            },
            error: function(data){
                console.log('Error');
                $('.user-api-errors').html("Can't connect to server, please try agane")
                console.log('Error---create');
            }

        })
    },
    setPropertyAddress: function(){

        if(!this.attributes.email){
            var prop_id = this.attributes.id;

            var el_property = _.find(Realync.properties_collection.models, function(prop_element){

                return prop_element.get("id") == prop_id;
            })
            console.log(this.attributes);

            if(el_property){
                this.attributes.myProperty = true;
            }else{
                this.attributes.myProperty = false;
            }

            var addr_string = this.get("address");
            console.log(addr_string);
            console.log(this);
            if(addr_string){
                return (addr_string.street1 + " " + addr_string.city + " " + addr_string.state + " " + addr_string.zip)
            }else{
                return "No property"
            }
        }else{
            var addr_string = this.get("profile");
            if(addr_string){
                return ("Searching in " + addr_string.city + " " + addr_string.state + " " + (addr_string.zip ? addr_string.zip : " ") )
            }else{
                return "Search address not set"
            }
        }
    },

  get_params_by_zip: function(data, role){

        console.log(data);
      if(! data.zipcode){
          data.zipcode = "";
      }
      $.ajax({
          url: "http://maps.googleapis.com/maps/api/geocode/json?address="+data.zipcode+"&sensor=true&language=en-US",
          type: 'GET',
          success: function(data_r){

              console.log('Ok');
              console.log(data_r);
              console.log(data);
            if(data_r.status !="ZERO_RESULTS" ){
                console.log(data_r.results[0]);
                var address_components = data_r.results[0]['address_components'] 
                for(val in address_components){
                    i = address_components[val];
                    console.log(i);
                    if(i.types[0] == "administrative_area_level_1") {
                        data.state =  i['short_name'];
                    }
                    if(i.types[0] == "locality") {
                        data.city =  i['long_name'];
                    }

                }
                data.longitude =  data_r.results[0]['geometry']['location']['lat'].toString();
                data.latitude =  data_r.results[0]['geometry']['location']['lng'].toString();
                if(role == 'client'){
                    console.log('------------client-------profile---------------');
                    console.log(data);

                    user_model.update_client_profile(data, "client")
                }else if(role == 'agent') {
                    console.log('------------agent----------------------');
                    user_model.update_agent_profile(data, "agent")
                }else if(role == 'createProperty') {
                    console.log(data);
                    user_model.createProperty(data);
                }else if(role == 'updateProperty') {
                    var prop = new Realync.Models.Property;
                    console.log("=--23-12-421-4-124-12-4124");
                    console.log(data.property_id);
                    prop.update_property(data, data.property_id);
                }


             }else{
                console.log("ZERO_RESULTS")
             }
           },
          error: function(data_r){
//                  alert.log('Error');
          }
      })


      return 111;

  },

    logout:function(){
      console.log('logout');
        $.ajax({
            url: localStorage.url + '/session/destroy',
            headers: {

            },
            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){

                console.log(output);
            },
            error: function(data){
                console.log('Error');
            }

        })
    },

    delete_account: function(){
        console.log('Delete Account');
        $.ajax({
            url: localStorage.url + '/user/delete',
            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
            },
            error: function(data){
                console.log('Error');
            }

        })
    },

    unread_message_count: function(){
        $.ajax({
            url: localStorage.url + '/user/unreadMessageCount',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                if (output.unreadMessageCount != 0){
                    $("#unread-messages").html("");
                    $("#unread-messages").append(output.unreadMessageCount).show();
                }else{
                    $("#unread-messages").html("").hide();
                }
            },
            error: function(data){
                console.log('Error');
            }

        })
    },

    change_password: function(data){
        console.log('----------Change Password-----------');
        console.log(data);

        $.ajax({
            url: localStorage.url + '/user/changePassword',

            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
                window.location = "dashboard#!/"
            },
            error: function(data){
                console.log('Error');
            }

        })
    },

    update_client_profile: function(data, role){

        console.log('--------update---client--------profile-----------')
        console.log(data)
        $.ajax({
            url: localStorage.url + '/user/editProfile',

            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');

                console.log(output);
                user_model.sunc_profile();
            },
            error: function(data){
                console.log('Error');
            }

        })

    },



   startSubscription: function(data){
        $.ajax({
            url: localStorage.url + '/user/startSubscription',
            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
                console.log(output.status);
                console.log(output.message);

                $('.response_answer').html(output.message);


            },
            error: function(data){
                $('.response_answer').html(output.message);
            }

        })

    },

    changeSubscription: function(data){
        $.ajax({
            url: localStorage.url + '/user/changeSubscription',
            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output);
                console.log(output.status);
                console.log(output.message);
                if(output.status == 'success'){
                    console.log('Ok');
                    $('.response_answer').html(output.message);

                }else if(output.status == 'failed'){
                    console.log('Fail');
                    $('.response_answer').html(output.message);

                }

            },
            error: function(data){
                $('.response_answer').html(output.message);
            }

        })

    },

    update_agent_profile: function(data, role){

        console.log('--------update---agent--------profile-----------')
        console.log(data)
        delete data['searchRadius']
        delete data['bathCount']
        delete data['bedCount']
        delete data['priceMax']
        delete data['priceMin']
        delete data['sqft']
        $.ajax({
            url: localStorage.url + '/user/editAgentProfile',
            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');

                console.log(output);
                user_model.sunc_profile(role);
            },
            error: function(data){
                console.log('Error');
            }

        })

    },
    
    subscription: function(type){
        console.log(type);
    },

    sunc_profile:function(role){
        console.log('--------sunc----------profile-----------')
        var session = new Realync.Models.Session();
        var id = JSON.parse(localStorage.user).id ;

        if(role == "agent"){
            session.get_profile_info(id, "none");
        }else{
            if(this.sign_up){
                session.get_profile_info(id, true);
                this.sign_up = false;
            }else{
                session.get_profile_info(id, false);
            }
        }
    },

    findAgent: function() {
        console.log("findAgent");
        $.ajax({
            url: localStorage.url + '/user/findAgent',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(data) {
                var profile = JSON.parse(localStorage.profile_info);
                var view = new Realync.Views.Agents;
                var arr = data.agents;
                Realync.agent_collection.reset(arr);
                view.render();
                console.log("success");
                if (data.message == "No search radius or location set") {
                    $('.title-text-all-agents').append('<div id="update_profile">Please update your search params in <a href = "#!/profile">Profile</a>  </div>')
                }
                console.log(data);

            },

            error: function(data) {
                console.log("error");
            }
        })
    },

    setAgent: function(data_r) {

        $.ajax({
            url: localStorage.url + '/user/setAgent',

            type: 'GET',

            data: data_r,

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("Set agent!!");
                console.log(data);
               if(data.status == "SUCCESS") {
                   user_model.update_profile_after_leaveAgent("set");
               }
            },
            error: function(data) {
                console.log("error set agent");
            }
        });

    },

    getAgentProfile: function(id) {
      console.log("agent profile");
        $.ajax({
            url: localStorage.url + '/user/profile/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("AgentProfile");
                var agentProfile = new Realync.Models.Profile(data.profile);
                var property = new Realync.Models.Property;
                property.get_agent_property(id);
                (new Realync.Views.MyAgentProfile({model: agentProfile})).render(agentProfile);
                console.log(agentProfile);
            },
            error: function(data) {
                console.log("agentProfile error");
            }
        })
    },

    getAgentProfileSearch: function(id) {
        console.log("agent profile search");
        $.ajax({
            url: localStorage.url + '/user/profile/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("AgentProfile Search");
                var agentProfile = new Realync.Models.Profile(data.profile);
                (new Realync.Views.SearchAgentProfile({model: agentProfile})).render(agentProfile);
                var property = new Realync.Models.Property;
                property.get_agent_property(id);

            },
            error: function(data) {
                console.log("agentProfile search error");
            }
        })
    },


    getClientProfile: function(id) {
      console.log("client profile");
        $.ajax({
            url: localStorage.url + '/user/profile/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("ClientProfile");
                var client = new Realync.Views.ClientProfile({model: clientProfile});
                var clientProfile = new Realync.Models.Profile(data.profile);
//                var id = data.profile.myAgent.id;
                client.render(clientProfile);
                var ul = $('#client_agent-info');
                if(!$.isEmptyObject(clientProfile.attributes.myAgent)) {
                    var agent = new Realync.Models.MyAgent(clientProfile.attributes.myAgent);
                    var view = new Realync.Views.Client_agent_info({model: agent});
                    ul.html(view.render().el);
                } else {
                    ul.html(clientProfile.attributes.user.firstName +
                        " isn`t working with an agent.")
                }
                var tour = new Realync.Models.Tour;
                tour.getClientTours(id);
            },
            error: function(data) {
                console.log("clientProfile error");
            }
        })
    },

    leaveAgent: function() {
        console.log("leave agent");
        $.ajax({
            url: localStorage.url + '/user/leaveAgent',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("Leave agent!!");
                if(data.status == "SUCCESS") {
                    user_model.update_profile_after_leaveAgent("leave");
                }
            },
            error: function(data) {
                console.log("leave agent error");
            }
        })
    },


    update_profile_after_leaveAgent: function(type_r) {
        id = JSON.parse(localStorage.user).id;
        console.log("update profile");
        $.ajax({
            url: localStorage.url + '/user/profile/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("update profile after leaveAgent");
                data.profile.firstName =  data.profile.user.firstName;
                data.profile.lastName =  data.profile.user.lastName;
                data.profile.bathcount =  data.profile.bathCount;
                if(!data.profile.activelySearching) {
                    data.profile.activelySearching = false;
                }

                localStorage.profile_info = JSON.stringify(data.profile)
                if(!data.profile || data.profile =="undefined"){
                    localStorage.profile_info = JSON.stringify({});
                }
                console.log(type_r);
                if (type_r == "set") {
                    window.location = "#!/";
                } else if(type_r == "leave") {
                    Backbone.history.loadUrl();
                    return false;
                }
            },
            error: function(data) {
                console.log(type_r);
                if (type_r == "set") {
                    window.location = "#!/";
                } else if(type_r == "leave") {
                    window.location = "#!/agents";
                }
                console.log("agentProfile error");
            }
        })
    },

    createProperty: function (data) {

        $('#property-progres-bar').css('display','block');
        $('#prop-cover-photo').css('z-index','5');
        $('#prop-multy-progress').css('z-index','4');
        $('#prop-video-progress').css('z-index','3');

        if( $('.video_upload').data()){
            var arr = $('.video_upload').data().files[0].name.split('.')
            arr.splice(arr.length-1, 1);
            var filename = ''
            for(i in arr){
                filename += arr[i]
            }
            data['videoName'] = filename;
        }else{
            data['videoName'] = "no_video";
        }

        set_progres('save property', 'image');
        set_spiner();
        $.ajax({
            url: localStorage.url + '/property/create',

            type: 'POST',

            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                set_progres('save cover photo', 0);
                console.log("SUCCESS create prop");
                console.log(data);
                window.location = "#!/property/" + data.propertyID;


                $('#fileupload').fileupload({
                  url: localStorage.url + "/property/addPhoto/" + data.propertyID
                });
                $('#multifileupload').fileupload({
                    url: localStorage.url + "/property/addPhoto/" + data.propertyID
                });
                localStorage.new_prop_id = data.propertyID;
                var property = new Realync.Models.Property;
                property.index('new_prop');

                console.log($('#prop-multy-photo').length)
                console.log($('#prop-multy-photo').length)
                console.log($('#prop-multy-photo').length)
                if( $('.multiple_upload').length == 0){
                    $("<style>#prop-multy-photo { z-index: 1; }</style>").appendTo(document.documentElement);

                }else{
                    $('.multiple_upload').each(function(){
                        $(this).data().submit();
                    });
                }

                if( $('.btn.btn-primary.cover-photo').length == 0){

                    $("<style>#prop-cover-photo { z-index: 1; }</style>").appendTo(document.documentElement);
//                    $('#prop-cover-photo').css('z-index','1');
                }else{

                   set_process_text('start load cover photo', 'cover');
                   $('.btn.btn-primary.cover-photo').data().submit();
                }

                if( $('.video_upload').length == 0){
                    $("<style>#prop-video-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                    $('#prop-video-photo').css('z-index', 1);
                }else{
                    var user_id = JSON.parse(localStorage.profile_info).user.id;
                    user_model.upload_to_s3(filename, data.propertyID, user_id);
                }


                reset_progres();
                reset_spiner();
                check_downloades();
            },
            error: function(data) {
                reset_progres();
                reset_spiner();
                console.log("error create prop");
            }
        });
    },

    upload_to_s3: function(filename, property_id, user_id){
        console.log('from server - params');
        console.log(filename);
        console.log( property_id);
        console.log(user_id);

        $.ajax({
            url: '/documents/generate_url',
            type: 'POST',
            data: {
                user_id: user_id,
                property_id: property_id,
                filename: filename
            },
            success: function(data){
                console.log('from server');
                console.log(data);
                console.log('from server');

                var video_file = $('.video_upload').data().files[0];

                 uploadToS3(video_file, data.put_url, data.content_type)


            },
            error: function(data){
                console.log(data)
            }
        })
    },
    add_video_to_property: function(filename, propertyID, user_id){
        $.ajax({
            url: localStorage.url +  '/property/addVideo/' + propertyID,

            type: 'POST',

            data: {
                videoName: 'https://s3-us-west-2.amazonaws.com/videos-s/' + user_id +'/' + propertyID + '/video/' + filename
            },

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("SUCCESS Set Video for prop");
                console.log(data);

            },
            error: function(data) {
                console.log("error set video for prop");
            }
        });

    },


    find_users: function(param){
        $.ajax({
            url: localStorage.url + '/user/searchUserList',

            type: 'POST',

            data: {
                searchTerm: param
            },

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("SUCCESS create prop");
                var users = data.users;
                console.log(users);
                Realync.search_user_list_collection.reset();
                for(i in users){
                    var user = new Realync.Models.User(users[i])
                    Realync.search_user_list_collection.add(user);
//                    console.log(users[i]);
                };
                var view = new Realync.Views.Search_user_list;
                view.render();
            },
            error: function(data) {
                console.log("error create prop");
            }
        });
    },

    compose_message: function(data){
        console.log(data);
        console.log(data);
        $.ajax({
            url: localStorage.url + '/message/compose',

            type: 'POST',

            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("SUCCESS create prop");
                var message_id = data.createdMessage.id
                console.log(data.createdMessage.id);


                var timeout_func = function(){window.location = '#!/update_messages/' + message_id}
                setTimeout(timeout_func, 500);

            },
            error: function(data) {
                console.log("error create prop");
            }
        });
    },

    validate: function(attrs, options) {

  }

});
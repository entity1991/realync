Realync.Models.Session = Backbone.Model.extend({
    initialize: function() {
        _.bindAll(this, 'login');
        this_model = this;
    },
    defaults: {
        username: '',
//        email: '',
        password: ''
    },

    schema: {
        email: {

            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Email'
            },
            title: null

        },
        mobile: {
            type: 'Hidden',

            editorAttrs: {
                'placeholder': 'Password'
            },
            title: null,
            value: false
        },
        password: {
            type: 'Password',
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Password'
            },
            title: null
        }
    },
//    url: 'http://stage.realync.com/session/create'
    get_profile_info:function(id, redirect){
        console.log('get-----profile----info--------')
        console.log(localStorage.url + 'user/profile/'+id+ '')
        $.ajax({
            url: localStorage.url + '/user/profile/'+id+ '',
//            url: 'http://192.168.2.190/session/create',
            type: 'GET',
            data: {mobile: true},
            xhrFields: {
                withCredentials: true
            },
            success: function(data){

                console.log(data);
                console.log('-------------sunc profile------------------------');
                data.profile.firstName =  data.profile.user.firstName;
                data.profile.lastName =  data.profile.user.lastName;
                data.profile.bathcount =  data.profile.bathCount;
                if(!data.profile.activelySearching) {
                  data.profile.activelySearching = false;
                }

               localStorage.profile_info = JSON.stringify(data.profile)
               if(!data.profile || data.profile =="undefined"){
                 localStorage.profile_info = JSON.stringify({});
               }


               if(window.location.hash == "#!/profile"){
                    window.location = "/dashboard#!/"
               }else if(redirect == "none"){
                   var view = new Realync.Views.updateSubscription();
                   view.render();
                   window.location.hash = "!/updateSubscription";
               }else if(redirect){
                    window.location = "/dashboard#!/"
               }


             },
            error: function(data){
//                alert('Error')

                console.log('Error');
            }

        })

    },

    resetPassword: function(email){
        $.ajax({
            url: localStorage.url + '/user/forgotPasswordEmail/' + email,
            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(data){
                console.log(data)
                if(data.status == 'success'){
                    $('.user-api-errors').html(data.message);

                    $("#forgotPasswordUsername, #button_resetPass, #forgotPassBackButton").hide();
                    $("#sign_in_field, #sign_in, .button_resetPass").show();
                }else{
                    if(data.status == 'failed'){
                        $('.user-api-errors').html(data.message)
                    }
                };

            },
            error: function(data){
                console.log("Bad")
            }
        })
    },

    resetConfirmation: function(token){
        $.ajax({
            url: localStorage.url + '/user/resetConfirm/' + token,
            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(data){
                console.log(data)
                if(data.status == 'success'){
                    $('.user-api-errors').html(data.message);
                }else{
                    if(data.status == 'failed'){
                        $('.user-api-errors').html(data.message)
                    }
                };

            },
            error: function(data){
                console.log(data)
            }

        })
    },

    login:function(data){
        console.log('---------------login-------------------------');
        $.ajax({
            url: localStorage.url + '/session/create',
//            url: 'http://192.168.2.190/session/create',
            type: 'POST',
            data: data,
            xhrFields: {
                withCredentials: true
            },
            success: function(data){

                 if(data.status == 'SUCCESS'){
                       var user =  data.User;
                       console.log(data.User.id)
                       var user_id = data.User.id
                       this_model.get_profile_info(user_id, true)
                       localStorage.user = JSON.stringify(user);
                       //window.location = "/dashboard#!/"
                 }else{
                   if(data.status == 'failed'){

                       $('.user-api-errors').html(data.message)
                   }
                 };
            },
            error: function(data){
                console.log('Error');
            }

        })
    }
});

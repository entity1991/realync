Realync.Collections.Upcoming = Backbone.Collection.extend({
    initialize: function() {
        _.bindAll(this, "sort_by_days")
    },

    model: Realync.Models.Upcoming,
    url: function(){
        console.log('create tours');
    },

    sort_by_days: function(){
        var hash = [];

        for(i in this.models ){
           var start_date = this.models[i].get("startDate");
           var day = new Date(start_date).getDate();
           var month = new Date(start_date).getMonth();
           var year = new Date(start_date).getFullYear();

           var sortDay = $.grep(hash, function( d, m ) {
                return ( d.day == day && d.month == month );
           });

            if(sortDay.length != 0){
                sortDay[0].tours.push(this.models[i]);
            }else{
                hash.push( {day: day,
                            month: month,
                            year: year,
                            tours: [this.models[i]]});
            }
        }

        return hash
    }
});
Realync.upcoming_collection = new Realync.Collections.Upcoming();

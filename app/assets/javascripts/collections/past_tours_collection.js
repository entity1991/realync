Realync.Collections.PastTours = Backbone.Collection.extend({
    initialize: function() {
        _.bindAll(this, "sort_by_days")
    },

    sort_by_days: function(hash){
        global_hash = []

        for(i in this.models ){
            var start_date = this.models[i].get("startDate");
            var day = new Date(start_date).getDate();
            var month = new Date(start_date).getMonth();
            var year = new Date(start_date).getFullYear();

            var sortDay = $.grep(hash, function( d, m ) {
                return ( d.day == day && d.month == month );
            });

            if(sortDay.length != 0){
                sortDay[0].tours.push(this.models[i]);
            }else{
                hash.push( {day: day,
                    month: month,
                    year: year,
                    past: true,
                    tours: [this.models[i]]});
            }
        }
        for(i in hash){
            hash[i].tours = _.sortBy(hash[i].tours, function(tour){ return tour.get('startDate')})
        }
        global_hash = hash

        return hash
    },

    model: Realync.Models.Tour,
    url: function(){
        console.log('create tours');
    }
});
Realync.past_tours_collection = new Realync.Collections.PastTours();

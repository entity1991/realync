Realync.Views.viewProperty = Backbone.View.extend({
    initialize: function(){
      this_view = this
    },

    template: JST['properties/viewProperty'],
    el: "#page-content-bb",

    events: {
    	"click #delete" : "delete_property",
        "click .schedule_tour, #schedule_click_calendar" : "schedule_tour",
        "click .play_video" : "play_video",
        "click .close_video" : "close_video",
        "click #share_tweet" : "share_tweet",
        "click #request_tour" : "request_tour_property"
    },

    request_tour_property: function() {
        var view = new Realync.Views.viewProperty;
        this.model.request_tour(this.model.attributes.id)
    },

    share_tweet: function() {
        console.log('TWEET')
        console.log(this.model);
        var url = 'https://twitter.com/intent/tweet?';
        url +=  'text=' + encodeURIComponent(this.model.get('address').street1 + ' ' + this.model.get('address').city + ', ' + this.model.get('address').state + ', ' + this.model.get('address').zip
            +'\n' + window.location.href + '\n');
        url +=  '&url='+encodeURIComponent(this.model.get('picURL'));
        var left  = ($(window).width()/2)-(600/2), top   = ($(window).height()/2)-(300/2)
        popup = window.open (url, "popup", "width=600, height=300, top="+top+", left="+left);

        return false;
    },

    close_video : function() {
        $("#video_view").hide();
        $(".close_video").hide();
        var v = document.getElementsByTagName("video")[0];
        v.pause();
        reset_spiner();
    },

    play_video : function() {
        var v = document.getElementsByTagName("video")[0];
        if (v.currentTime > 0) {
            v.currentTime = 0;
        }
        $("#video_view").show();
        $(".close_video").show();
        set_spiner();
    },

    schedule_tour: function(){
        localStorage.property_schedule_id = this.model.get("id");
        window.location = "#!/createTour";
        return false;
    },

    show_tours: function(){
        var prop_tours = Realync.properties_collection.getPropertyTours(this.model.get("id"));
        $(".property_tour_container").html("");

        for (i in prop_tours){
            var view = new Realync.Views.PropertyTours({model: prop_tours[i]});
            $(".property_tour_container").append(view.render().el);

        }
    },

    delete_property: function () {
    	var prop = new Realync.Models.Property;
    	var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];
    	prop.delete(id);
    },

    facebook_share: function(){
        window.fbAsyncInit = function() {
            FB.init({appId: '780441068656667', status: true, cookie: true,
                xfbml: true});
        };
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());

//        $(document).ready(function(){
            $('#share_button').click(function(e){
                e.preventDefault();
                FB.ui(
                    {
                        method: 'feed',
                        name: $.trim($('.view_property_header h1').text()),
                        link: window.location.href,
                        picture: this_view .model.get('picURL'),
                        caption: 'BED: ' + $.trim($('.count-bed h5').text())+ ', ' + 'BATH: ' + $.trim($('.count-bath h5').text())+ ', ' +
                            'SQ FEET: ' +  $.trim($('.count-sqft h5').text()) ,
                        description: this_view .model.get('description'),
                        message: ''
                    });
            });
//        });

    },

    show_gallery: function() {
        var photos = this.model.get('photoURLs');
        $('ul#gallery_items').html('');

        for (i in photos){
            var view = new Realync.Views.Property_gallery_item({model: {url: photos[i]}}); 
            $('ul#gallery_items').append(view.render().el);
        }
        console.log(photos.length);
        if(!photos.length) {
            $('#property_gallery').hide();
        }
        if(photos.length < 5) {
            $('#property_gallery').carousel({
                disabled: true
            });
            $('#property_gallery button').hide();
        }
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        var ul = $('#request_tour');
        if (JSON.parse(localStorage.profile_info).user.id != this.model.attributes.realtor) {
            $('.if_owner').remove();

            ul.html('<div class="button button-transparent-white speciall_div">Request Tour</div>')

        }
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $(".cover-photo").css("background-image", "url(" + this.model.attributes.picURL + ")");
        this.show_tours();
        this.show_gallery();
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.properties').children().addClass('active_link_dash');
        var view = new Realync.Views.Video_view({model: this.model});
        $("#video_view").append(view.render().el);
        $(document).foundation();
        this.facebook_share();

    }

});
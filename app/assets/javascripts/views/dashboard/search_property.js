Realync.Views.SearchProperty = Backbone.View.extend({

    template: JST['dashboard/search_property'],

    events: {
      "click .schedule_tour_search_property" : "schedule_tour_prop",
      "click .view_search_property" : "view_property"
    },

    view_property: function(){
        var property_id = this.model.get("id");
        window.location = "/dashboard#!/property/" + property_id;
        return false
    },

    schedule_tour_prop: function(){
        localStorage.property_schedule_id = this.model.get("id");
        window.location = "#!/createTour";
        return false;
    },


    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        console.log("create search property");
        return this;
    }

});
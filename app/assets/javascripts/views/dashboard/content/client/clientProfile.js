Realync.Views.ClientProfile = Backbone.View.extend({

    template: JST['dashboard/content/client/clientProfile'],
    el: "#page-content-bb",

    events: {
        "click #message_me": "message_to_client"
    },

    message_to_client: function(){
        localStorage.send_users_arr =  JSON.stringify([this.model.get("user").name]);
        window.location = "#!/messages";
        return false;
    },

    render: function (model) {
        this.model = model;
        $(this.el).html(this.template(model.toJSON()));
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    }

});
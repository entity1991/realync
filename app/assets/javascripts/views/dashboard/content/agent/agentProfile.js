Realync.Views.AgentProfile = Backbone.View.extend({

    template: JST['dashboard/content/agent/agentProfile'],
    el: "#page-content-bb",

    events: {
        "click .request_connection" : "request_connection"
    },

    request_connection: function() {
        var user = new Realync.Models.User;
        var params = this.model.attributes;

            var data = {agent: '{                                \
            "id": "'+ params.id + '",                        \
            "name":"' + params.name + '",                    \
            "profilePicUrl": "'+  params.mobilePicUrl + '",  \
            "email":"' + params.email + '"                   \
        }'};


        user.setAgent(data);
        return false;
    },

    render: function (model) {
        this.model = model;
        $(this.el).html(this.template(model.toJSON()));
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    }

});
Realync.Views.Sharing = Backbone.View.extend({

    template: JST['dashboard/content/settings/sharing'],
    el: "#account-content-bb",

    render: function () {
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $("a.shar_link").css({
        	'color' : '#1ca5c9',
        	'text-decoration' : 'underline'
        	});
        $("a.sub_link").css('color', '#a4a4a4');
        $("a.prof_link").css('color', '#a4a4a4');
        $("a.acount_link").css('color', '#a4a4a4');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.settings').children().addClass('active_link_dash');
    }
});
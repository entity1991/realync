Realync.Views.CardView = Backbone.View.extend({

    template: JST['dashboard/content/settings/card_view'],
    tagName: 'div',

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        console.log("create date list");
        console.log(this.model);
        return this;
    }

});
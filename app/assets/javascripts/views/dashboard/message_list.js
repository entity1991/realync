Realync.Views.MessageList = Backbone.View.extend({

    template: JST['dashboard/content/message_list'],
    tagName: 'li',

    check: function () {
        console.log('tada');
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;    }
});
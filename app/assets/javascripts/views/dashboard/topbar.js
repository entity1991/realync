/**
 * Created by entity on 4/1/14.
 */
Realync.Views.Topbar = Backbone.View.extend({

    template: JST['dashboard/topbar'],
    el: "#topbar-bb",

    events: {
        "click *": "check"
    },

    check: function () {
        console.log('topbar ckicked');
    },

    addSelector: function(){
        $("#search").select2({
            multiple: true,
            minimumInputLength: 1,

            ajax: {
                url: localStorage.url + '/user/searchUserAndProperty',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                crossDomain: true,
                dataType: 'json',


                data: function (term, page) {
                    console.log(term);
                    return {

                        searchTerm: term //search term

                    };
                },
                results: function (data, page) {
                    console.log(data);
                    var term_result = '"' + $('.select2-input').val() + '"';
                    var link_for_all = "<div class='link_for_result'><a>View All Results for " + term_result + "</a></div>"

                        $('.link_for_result, #result-for-term').html("");
                        $('#select2-drop').append(link_for_all);


                        $('.link_for_result').on('click', function(){

                            if(Backbone.history.fragment == "!/search"){
                                (new Realync.Views.SearchResult).render();
                                $('#result-for-term').append(term_result);
                            }else{
                                window.location = '/dashboard#!/search'
                                $('#result-for-term').append(term_result);
                            }
                            $("#select2-drop").remove();
                            $("#select2-drop-mask").remove();
                        })

                    Realync.search_collection.reset();
                    var search_arr = $.merge( data.users, data.properties );

                    for( i in search_arr){
                        var model_search = new Realync.Models.User(search_arr[i]);
                        Realync.search_collection.add(model_search);
                    }

                    return {results: search_arr};
                }
            },

            formatResult: movieFormatResult,
            dropdownCssClass: "bigdrop",
            formatNoMatches: formatNotFound,
            escapeMarkup: function (m) { return m; }
        }).on("change", function(ev) {

            var agent_realtor;
            var id;

            if (ev.added.sqft != undefined) {
                var property_id = ev.added.id;
                window.location = "/dashboard#!/property/" + property_id;
            } else if (ev.added.realtor != undefined) {
                agent_realtor = ev.added.realtor;
                id = ev.added.id;
            } else  {
                agent_realtor = undefined;
            }
            var view  =  new Realync.Views.SearchResult;
            view.view_profile(id, agent_realtor);
        });


        function formatNotFound(term) {
            console.log(term);
            console.log(term);
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            if(validateEmail(term)){
                return "<div class='search_div'>" + "<b>" + term + "</b>" + '? We don`t know that person. Add their name and select "Invite User". '
                    + 'We`ll send them an invite email. Once they sign up, you`ll be added as their agent and they`ll recieve an invite to access your tour. '
                    + "</div>";
            }else{
                return  "<div class='search_div'>" + "We`re sorry. There are no results for " + "<b>"+ term +"</b>" + "</div>"
            }


        }
        function movieFormatResult(user) {
            console.log(user);

            if(user.email){
                var markup = "<div class='movie-result'>";
                if(user.mobilePicUrl == "/images/noPhoto.jpeg"){
                    user.mobilePicUrl = localStorage.url + "/images/noPhoto.jpeg";
                }
                markup += "<div class='user-avata'><img class='round' src='" + user.mobilePicUrl + "'/></div>";
                markup += "<div class='user-name'><div>" + user.name + "</div></div>";
                markup += "</div>";

            }else{
                var markup = "<div class='movie-result'>";
                if(user.mobilePicURL == "/images/noPhoto.jpeg"){
                    user.mobilePicURL = localStorage.url + "/images/noPhoto.jpeg";
                }
                markup += "<div class='user-avata'><img class='round' src='" + user.mobilePicURL + "'/></div>";
                markup += "<div class='user-name'><div>" + user.address.street1 + " "
                                                         + user.address.city + " "
                                                         + user.address.state + " "
                                                         + user.address.zip + "</div></div>";
                markup += "</div>";
            }
            return markup

        }
    },

    addProfileInfo: function(){
        var user = {}
        if( localStorage.user){
         user = JSON.parse(localStorage.profile_info).user;
        }
        var user = new Realync.Models.User(user );

        var view = new Realync.Views.TopProfileTemplate({model: user});
        $('#profile').html(view.render().el);
    },

    render: function () {
        $(this.el).html(this.template());
        if(localStorage.profile_info && !JSON.parse(localStorage.profile_info).user.realtor) {
            $('input#search').attr('placeholder','Search properties and agents');
        };
        this.addProfileInfo();
        this.addSelector();
    }
}); 
/**
 * Created by vitaliyjorzh on 09.04.14.
 */
Realync.Views.agentBrokers = Backbone.View.extend({

    template: JST['landing/agentBrokers'],
    el: "#content-bb",

    render: function () {
    	$(".landing-image1, .landing-template1").remove(); // Temporarily
        this.$el.html(this.template(this.template()));
        $('.footer-links a, .top-bar-landing-links a').removeClass('active_link');
        $('a.agent_link').addClass('active_link');
        return this;
    }
});
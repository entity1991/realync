Realync.Views.CreateTour = Backbone.View.extend({

    template: JST['tours/createTour'],
    el: "#page-content-bb",

    events: {
        "click .open_house, .scheduled" : "open_house",
        "click #submit_open_house" : "submit_open_house"
    },

    submit_open_house: function(){
        $("form#new_tour").submit()
    },

    open_house: function(){
        console.log("OPEN HOUSE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        var el = $("#open_house_block")
        var show = $("input[name = 'tour']:checked").val();
        if(show != "false"){
            el.css("display", "none");
            $("#tour_host").hide();
            $("#attendees_field").fadeIn();
            $("#submit_form").show();

        }else{
            el.css("display", "block");
            $("#attendees_field").hide();
            $("#submit_form").hide();
            $("#tour_host").fadeIn();
        }

    },

    addPropertyItem: function(property) {
       var availableTags = [];
       availableTags.push(property.id);
        console.log(availableTags);
        console.log(Realync.properties_collection);

        console.log("mayby here???");
        var view = new Realync.Views.Select_property({model: property});
        $('.helpProperty').append(view.render().el);
    },



    addProfileInfo: function(){
        var user = {}
        if( localStorage.user){
            user = JSON.parse(localStorage.profile_info).user;
        }
//        var user = new Realync.Models.User(user );

        var li = '<div>';
        li += "<div class='movie-result'>";
        li += "<div class='attendee-wrap'>";
        li += "<img class='round' src='" + user.mobilePicUrl + "'/>";
        li += "<div class='user-name' >" + user.name + "</div>";
        li += "<div class='user-id'><div style='visibility:hidden' id='" + user.id + "'></div></div>";
        li += "</div>";
        li += '</div>';

//        var view = new Realync.Views.TopProfileTemplate({model: user});
        $('#current-user').html(li);
    },

    render: function () {
        console.log('render  tour');
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');

        $('input[name="addressString"]' ).click(function() {
            $('.help').css("display", "block");
            alert( "Handler for .click() called." );
        });
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.tours').children().addClass('active_link_dash');
        this.addProfileInfo();

        window.fbAsyncInit = function() {
            FB.init({appId: '780441068656667', status: true, cookie: true,
                xfbml: true});
        };
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());

    }

});
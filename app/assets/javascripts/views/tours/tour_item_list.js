Realync.Views.TourItemList = Backbone.View.extend({

    template: JST['tours/tour_item_list'],
    events: {
        "click .send_message_to_attend" : "send_message",
        "click .start_live_tour" : "start_live_tour",
        "click .delete_tour" : "delete_tour",
        "click .join_tour" : "join_tour"
    },

    delete_tour: function(){
      console.log('delete');
      var id = this.model.get('id');
      $("#" + id).parents('.dashboard-tours-in').fadeOut();
      this.model.deleteTour(id);
      return false
    },

    start_live_tour: function(){
        window.location = "#!/live_tour/" + this.model.get("id");
    },

    join_tour: function(){
        window.location = "#!/live_tour/" + this.model.get("id");
        //this.model.createTourOpentok(this.model.get("videoSession"), "agent");
        return false;

    },


    send_message: function(){
        var arr_access_list = this.model.get("accessList");
        var arr = [];
        for (i in arr_access_list){
            if(arr_access_list[i].name){
                arr.push(arr_access_list[i].name);
            }
        }
        localStorage.send_users_arr =  JSON.stringify(arr);
        window.location = "#!/messages";
        return false;
    },

    render_style: function() {
 
    },

    render: function () {
        console.log(this.model);
        console.log(this.model);
        console.log(this.model);
        console.log(this.model);
        var agent = JSON.parse(localStorage.profile_info).user.id == this.model.get('realtor');
        this.model.set('agent', agent);
        this.$el.html(this.template(this.model.toJSON()));
        console.log("create tour item list");

        return this;
    }

});
Realync.Views.ShowTour = Backbone.View.extend({
    initialize: function(){
        this_view = this
    },

    template: JST['tours/showTour'],
    el: "#page-content-bb",
    events: {
        "click #share_tweet" : "share_tweet",
        "click .play_video" : "play_video",
        "click .close_video" : "close_video"
    },

    close_video : function() {
        $("#video_view").hide();
        $(".close_video").hide();
        var v = document.getElementsByTagName("video")[0];
        v.pause();
        reset_spiner();
    },

    play_video : function() {
        console.log("ШОсь не паше");
        var v = document.getElementsByTagName("video")[0];
        if (v.currentTime > 0) {
            v.currentTime = 0;
        }
        $("#video_view").show();
        $(".close_video").show();
        set_spiner();
    },

    share_tweet: function() {
        console.log('TWEET')
        console.log(this.model);
        var url = 'https://twitter.com/intent/tweet?';
        url +=  'text=' + encodeURIComponent(this.model.attributes.address.street1 + ' ' + this.model.attributes.address.city + ', ' + this.model.attributes.address.state + ', ' + this.model.attributes.address.zip
            +'\n' + window.location.href + '\n');
        url +=  '&url='+encodeURIComponent(this.model.attributes.property.picURL);
        var left  = ($(window).width()/2)-(600/2), top   = ($(window).height()/2)-(300/2)
        popup = window.open (url, "popup", "width=600, height=300, top="+top+", left="+left);

        return false;
    },

    render_loaut: function(){
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    },

    facebook_share: function(){
        window.fbAsyncInit = function() {
            FB.init({appId: '780441068656667', status: true, cookie: true,
                xfbml: true});
        };
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());

//        $(document).ready(function(){
        $('#share_button').click(function(e){
            e.preventDefault();
            FB.ui(
                {
                    method: 'feed',
                    name: $.trim($('.view_property_header h1').text()),
                    link: window.location.href,
                    picture: this_view .model.attributes.property.picURL,
                    caption: 'BED: ' + $.trim($('.count-bed h5').text())+ ', ' + 'BATH: ' + $.trim($('.count-bath h5').text())+ ', ' +
                        'SQ FEET: ' +  $.trim($('.count-sqft h5').text()) ,
                    description: this_view .model.attributes.property.description,
                    message: ''
                });
        });
//        });

    },

    render: function () {
        console.log('render  tour view');
        console.log(this.model);
        $(this.el).html(this.template(this.model.toJSON()));
        this.render_loaut();
        this.facebook_share();
        var view = new Realync.Views.Video_view({model: this.model});
        $("#video_view").append(view.render().el);



    }

});
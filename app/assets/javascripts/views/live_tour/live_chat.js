Realync.Views.LiveChat = Backbone.View.extend({

    template: JST['live_tour/livechat'],
    tagName: 'div',

    addChatItem: function(chat){
        var view = new Realync.Views.LiveChatItem({model: chat})
        $(".chatbody").append(view.render().el);
    },

    view_all_chat: function(){
        $(".chatbody").html("");
        Realync.live_chat_item_collection.each(this.addChatItem, this)
    },

    render: function () {
        console.log("Live chat");
        console.log(this.model);
        this.$el.html(this.template(this.model));
        return this;
    }
});